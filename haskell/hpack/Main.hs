{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs 
    let t = head args
    content <- readFile t
    let linesOfFiles = lines content
    print linesOfFiles
    
    -- >>= (mapM_ putStrLn) . lines

    {--
        putStrLn "Entrer texte: "
        x <- getLine 
        print x
    --}

