import Image (creerImage, enregistrerPnm)
import MarcheAleatoire (mad)
import System.Environment (getArgs, getProgName)
import System.Random (getStdGen)

main :: IO ()
main = do
  progName <- getProgName
  args <- getArgs
  if length args /= 3
  then do
    putStrLn $ concat ["usage: ", progName, " <outfile> <size> <steps>"]
    putStrLn $ concat ["e.g.: ", progName, " out.pnm 400 10000"]
  else do
    rng <- getStdGen 
    let outfile = head args
        [size, steps] = map (read :: String -> Int) (tail args)
        image = creerImage size size
    enregistrerPnm outfile $ mad image steps rng

