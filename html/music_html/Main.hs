{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import           Data.Text.Lazy (toStrict)
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import qualified Clay as C
import           Lucid

data Musics = Musics {
    _title :: T.Text, 
    _artist :: T.Text 
} deriving Show

instance FromRow Musics where
    fromRow = Musics <$> field <*> field

myCss :: C.Css
myCss = C.body C.? do
    C.background C.lightblue

myClassCss = C.div C.# C.byClass "myClass" C.? do
    C.background C.beige
    C.border     C.solid (C.px 1) C.black

formatMusic :: Musics -> String
formatMusic (Musics title artist) = 
    T.unpack (T.concat [title, " - ", artist])
    
myPage :: [Musics] -> Html ()
myPage myMusics = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ toStrict $ C.render myCss
            style_ $ toStrict $ C.render myClassCss
        body_ $ do
            h1_ "My List"
            div_ [class_ "myClass"] $ do
                ul_ $ mapM_ (li_ . toHtml) (map formatMusic myMusics)

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=mybd user=toto password='toto'"
    
    putStrLn "\n*** Nom des artistes & leur titre ***"
    music <- SQL.query_ conn "SELECT t.name, a.name FROM artists a, titles t WHERE a.id = t.artist" :: IO [Musics]
    mapM_ print music

    SQL.close conn

    renderToFile "test.html" $ myPage music

