{-# LANGUAGE OverloadedStrings #-}

import qualified Clay as C
import           Data.Text.Lazy (toStrict)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import           Lucid
import           System.Environment (getArgs, getProgName)

myCss :: C.Css
myCss = C.body C.? do
    C.background C.lightblue

myClassCss = C.div C.# C.byClass "myClass" C.? do
    C.background C.beige
    C.border     C.solid (C.px 1) C.black
    
myPage :: [T.Text] -> Html ()
myPage myLines = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            style_ $ toStrict $ C.render myCss
            style_ $ toStrict $ C.render myClassCss
        body_ $ do
            h1_ "My List"
            div_ [class_ "myClass"] $ do
                ul_ $ mapM_ (li_ . toHtml) myLines

main :: IO ()
main = do
    args <- getArgs
    if length args == 2
    then do
        let [input, output] = args
        file <- TIO.readFile input
        let fileLines = T.lines file
        renderToFile output $ myPage fileLines
    else do
        print 42

