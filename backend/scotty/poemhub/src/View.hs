{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute, writeRoute) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid
import qualified Model

-- TODO implement CSS styles
myCss :: C.Css
myCss = do
    C.a C.# C.byClass "aCss" C.? do
      C.textDecoration  C.none
      C.color           C.inherit
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black
        C.margin           (C.em 1) (C.em 1) (C.em 1) (C.em 1)
        C.width            (C.px 320)
        C.textAlign        C.center
        C.float            C.floatLeft

-- TODO implement route views

displayPoems :: Model.Poem -> Html ()
displayPoems (Model.Poem id author title year body) =
    a_ [class_ "aCss", href_ "/write"] $ do
        div_ [class_ "divCss"] $ do
            h3_ $ toHtml title
            p_ [class_ "pCss"] $ toHtml $  T.concat [author, "by (", T.pack $ show year, ")"]

mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ do
    title_ titleStr
    style_ $ L.toStrict $ C.render myCss
  body_ page

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do
  h1_ "Poem hub"
  div_ $ do
    a_ [href_ "/write"] "write a new poem"
    toHtml (mapM_ displayPoems poems)

writeRoute :: Lucid.Html ()
writeRoute = do
  h1_ "Write a new poem"
  form_ [action_ "/save", method_ "post"] $ do
    div_ $ do
      label_ "author: "
    div_ $ do
      input_ [name_ "author"]
    div_ $ do
      label_ "title: "
    div_ $ do
      input_ [name_ "title"]
    div_ $ do
      label_ "year: "
    div_ $ do
      input_ [name_ "year"]
    div_ $ do
      label_ "body: "
    div_ $ do
      textarea_ [name_ "body", rows_ "4"] ""

    input_ [type_ "submit"]
    a_ [href_ "/"] $
      button_ "Cancel"
    

