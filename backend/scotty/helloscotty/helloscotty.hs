{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Web.Scotty (scotty, get, post, html, param, rescue)
import           Lucid

main = scotty 3000 $ do

  get "/" $ html $ renderText $ html_ $ do
                head_ $ do
                    meta_ [charset_ "utf-8"]
                body_ $ do
                    h4_ "The helloscotty project !"
                    a_ [href_ "/hello"] "go to hello page"
    -- http://localhost:3000/

  get "/hello"
    --name <- param "name"
    -- html $ L.fromStrict $ T.concat ["Hello ", name, " !"]
    $ html $ renderText $ html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h4_ "Hello !"
            form_ [action_ "/hello2", method_ "get"] $ do
                input_ [name_ "txt", value_ "enter text here"]
                input_ [type_ "submit"]
            div_ $ do
                a_ [href_ "/hello"] "go to hello page"

  get "/hello2" $ do
        name <- param "txt"
        html $ renderText $ html_ $ do
            head_ $ do
                meta_ [charset_ "utf-8"]
            body_ $ do
                h4_ $ toHtml $ T.concat ["Hello ", name, " !"]
                form_ [action_ "/hello", method_ "post"] $ do
                    input_ [name_ "txt", value_ "enter text here"]
                    input_ [type_ "submit"]
                div_ $ do
                    a_ [href_ "/hello"] "go to hello page"