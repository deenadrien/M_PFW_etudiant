{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE OverloadedStrings          #-}

module Meteorites where

import           Control.Monad.IO.Class (liftIO)
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import           Data.Csv( decode, FromRecord, HasHeader( NoHeader ) )
import           Data.List (nub)
import qualified Data.Vector as V
import           GHC.Generics (Generic)

data Meteorite = Meteorite {
  classname :: !B.ByteString,
  latitude :: !Double,
  longitude :: !Double
} deriving (Generic, Show)

instance FromRecord Meteorite

readCsv :: String -> IO ([Meteorite])
readCsv filename = do
  lazyData <- liftIO $ B.readFile filename
  let strictData = L.fromStrict lazyData
      eitherData = decode NoHeader strictData :: Either String (V.Vector Meteorite)
  return $ case eitherData of Left _ -> []
                              Right v -> V.toList v

getClassnames :: [Meteorite] -> [B.ByteString]
getClassnames = nub . (map classname)

