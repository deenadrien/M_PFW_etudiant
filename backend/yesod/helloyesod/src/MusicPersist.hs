{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module MusicPersist where

import qualified Data.Text as T
import           Data.Maybe (maybe)
import           Database.Esqueleto 
import           Database.Persist.Sqlite (runSqlite)
import           Database.Persist.TH

dbName = "music.db" 

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Artist
  name T.Text
  deriving Show
Title
  artistId ArtistId
  name T.Text
  deriving Show
|]

initdb :: IO ()
initdb = runSqlite dbName $ do
  runMigration migrateAll
  radiohead <- insert $ Artist "Radiohead"
  insert $ Title radiohead "Paranoid android"
  insert $ Title radiohead "Just"
  ratm <- insert $ Artist "Rage against the machine"
  insert $ Title ratm "Take the power back"
  insert $ Title ratm "How I could just kill a man"
  maalouf <- insert $ Artist "Ibrahim Maalouf"
  insert $ Title maalouf "La porte bonheur"
  return ()

selectTitles :: T.Text -> IO [T.Text]
selectTitles str = runSqlite dbName $ do
  runMigration migrateAll
  titles <- select $
            from $ \(a, t) -> do
            where_ (t ^. TitleArtistId ==. a ^. ArtistId)
            where_ (t ^. TitleName `like` (val $ T.concat ["%", str , "%"]))
            return (a, t)
  return $ Prelude.map format titles
  where format (a, t) = T.concat [artistName (entityVal a), " - ", titleName (entityVal t)]

