
## build

```
$ nix-build
$ firefox result/index.html
```

## shell

```
nix-shell
cabal new-build
ln -s dist-newstyle/build/x86_64-linux/ghcjs-0.2.0/bmxriders-client-miso-0.1.0.0/c/bmxriders-client-miso/build/bmxriders-client-miso/bmxriders-client-miso.jsexe/all.js .
firefox index.html

cabal new-build
```

