{ pkgs ? import <nixpkgs> {} }:

let 

  rp_ref = "ea3c9a1536a987916502701fb6d319a880fdec96";  # 2018-04-17
  rp = import (fetchTarball "https://github.com/reflex-frp/reflex-platform/archive/${rp_ref}.tar.gz") {};

  app = rp.project ({ pkgs, ... }: {
    packages = {
      bmxriders_client_reflex = ./bmxriders_client_reflex;
    };
    shells = {
      ghcjs = ["bmxriders_client_reflex"];
    };
  });
 
  bmxriders_client_reflex_pkg = pkgs.stdenv.mkDerivation rec {
    name = "bmxriders_client_reflex";
    src = ./.;
    buildInputs = [ app pkgs.closurecompiler ];
    appdir = "${app.ghcjs.bmxriders_client_reflex}/bin/bmxriders-client-reflex.jsexe";
    buildPhase = ''
      ${pkgs.closurecompiler}/bin/closure-compiler ${appdir}/all.js --compilation_level=ADVANCED_OPTIMIZATIONS --jscomp_off=checkVars --externs=${appdir}/all.js.externs > all.js
    '';
    installPhase = ''
      mkdir -p $out
      cp -R static $out/
      cp index.html all.js $out/
    '';
  };

in

if pkgs.lib.inNixShell then app.shells.ghcjs else bmxriders_client_reflex_pkg

